#+LANGUAGE: en
#+TITLE: TaskJuggler infrastructure.
#+SUBTITLE: Setting up a TaskJuggler service..
#+DESCRIPTION: Taskjuggler needs a set of prerequisites and software environment to properly work.
#+KEYWORDS: project-management workflow
#+AUTHOR: Giovanni Biscuolo
#+EMAIL: g@xelera.eu

#+NAME: abstract
#+BEGIN_ABSTRACT
In this document we describe how to set up the software infrastructire needed to start a TaskJuggler server and use it to manage your projects via git. 
#+END_ABSTRACT

* Prerequisites

** Email account

   You need a dedicated SMTP and IMAP (or POP3) capable account on a mail server; the email address should be something like taskjuggler@your_company.com.

   You also have to set up this for aliases for the same email address:

   #+begin_example
   timesheets
   timesheet-request
   statussheets
   statussheet-request
   #+end_example

   The email server setup is out of scope here.

** TODO Host server

   The host server must be a local or VPS machine with Debian GNU/Linux as base OS and Guix as package manager.  Install Guix as documented in the Guix Manual.

*** TODO Taskjuggler user (with passwordless ssh)

    Create the =taskjuggler= user and home on the host server, that user (and home) will be user by all the TaskJuggler services and related utilities.

    Set up a password-less ssh connection from your working machine (or machines) to that account: this will be used to interact with the remote services and git repository.

*** TODO Taskjuggler Guix setup

    Be sure you have the right shell environment, in =.profile=:

    #+begin_src bash :exports none :mkdirp yes :tangle .profile
      # Set Guix env
      export GUIX_PROFILE="$HOME/.guix-profile"
      export PATH="$HOME/.config/guix/current/bin${PATH:+:}$PATH"
      export INFOPATH="$HOME/.config/guix/current/share/info:$INFOPATH"
      export GUIX_LOCPATH="${GUIX_PROFILE}/lib/locale"
      export TZDIR="${GUIX_PROFILE}/share/zoneinfo"

      . "$GUIX_PROFILE/etc/profile"
    #+end_src

    #+RESULTS:

    Create this manifest =.guix-manifest.scm=:

    #+begin_src scheme :exports none :mkdirp yes :tangle .guix-manifest.scm
      (specifications->manifest
       '("nss-certs"
	 "glibc-locales"
	 "git"
	 "git-crypt"
	 "tzdata"
	 ;; TaskJuggler
	 "ruby-taskjuggler"
	 ;; MTA, MUA and mail processor
	 "msmtp"
	 "getmail"
	 "procmail"))
    #+end_src

    Install the packages with =guix install -m .guix-manifest.scm=

    *WARNING*: ruby-taskjuggler is still not upstreamed, it must be installed from our custom repository (via =./pre-inst-env guix install ruby-taskjuggler=).

*** TODO Taskjuggler git repo
    
    Create a bare repo in =/home/taskjuggler/repo=:

    #+begin_src bash
      mkdir -p /home/taskjuggler/repo/projects.git
      cd /home/taskjuggler/repo/projects.git
      git init --bare
    #+end_src

    Create the working directory used by the TaskJuggler services in the =taskjuggler= home, cloning the above created repo in the =projects= directory:

    #+begin_src bash
      git clone repo/projects.git/ projects
    #+end_src
    
    Create the post-receive git hook =/home/taskjuggler/repo/projects.git/hooks/post-receive=:

    #+begin_src bash
      #!/bin/sh
      GIT_REPO=/home/taskjuggler/repo/projects.git
      PRODUCTION_WORK_TREE=/home/taskjuggler/projects
      PRODUCTION_BRANCH=master
      
      # Unset automatically set GIT directory
      unset GIT_DIR
      
      # Pull PRODUCTION_BRANCH
      echo "Hook: pulling ${PRODUCTION_BRANCH} branch in ${PRODUCTION_WORK_TREE}"
      git -C ${PRODUCTION_WORK_TREE} pull ${GIT_REPO} ${PRODUCTION_BRANCH}
      
      exec git update-server-info
    #+end_src

    The above hook will automatically check out the =repo/projects.git= repository content to =projects= directory.
    
    And now use the repository as remote for your projects:

    #+begin_src bash
      git remote add production ssh://taskjuggler@SERVER/home/taskjuggler/repo/projects.git
    #+end_src

*** TODO MTA agent

    FIXME: TaskJuggler does not support sending email using a different SMTP port when using =emailDeliveryMethod: smtp= ad is also not possible to use =emailDeliveryMethod: sendmail= providing a different sendmail command. (Using full fledged SMTP server in our first demo).
    
    Create the msmtp config file .msmtprc:

    #+begin_src conf
      # TaskJuggler mstmp configuration file ~/.msmtprc
      #
      # Set default values for all following accounts.
      defaults
      
      syslog on
      
      # Use the mail submission port 587 instead of the SMTP port 25.
      port 587
      
      # Always use TLS.
      tls on
      
      #
      # Softwareworkers account
      account softwareworkers
      host submission.meup.it
      from tj3-demo@softwareworkers.it
      auth on
      user tj3-demo@softwareworkers.it
      password <PASSWORD>
      
      # Set a default account
      account default : softwareworkers
    #+end_src

    Start a local minimal SMTP server:

    #+begin_src bash
      msmtpd --port 9025 --command 'msmtp -f %F' &
    #+end_src

*** Getmail

    This is the getmail config:

    #+begin_src conf
      [retriever]
      type = SimplePOP3SSLRetriever
      server = imap.meup.it
      username = USER
      port = 995
      password = PASSWORD
      
      [destination]
      type = MDA_external
      path = /var/guix/profiles/per-user/taskjuggler/guix-profile/bin/procmail

      [options]
      read_all = True
      delete = True
    #+end_src

    Interesting [[https://wiki.archlinux.org/title/Getmail][ArchLinux Getmail wiki page]].
    
*** Procmail

    Create the mail spool dir and maildir as =taskjuggler= user:

    #+begin_src bash
      mkdir -p $HOME/Mail/
    #+end_src

    This is the procmail config

    #+begin_src conf
      PATH=$HOME/.guix-profile/bin:/usr/bin:usr/sbin:/usr/local/bin:/usr/local/sbin:
      MAIL=$HOME/var/spool/mail/
      MAILDIR=$HOME/Mail/
      DEFAULT=$HOME/Mail/all
      LOGFILE=$MAILDIR/procmail.log
      SHELL=/bin/bash                            
      PROJECTDIR=/home/taskjuggler/projects
      CONFIG=.taskjugglerrc
      LANG=en_US.UTF-8
      LC_ALL=en_US.UTF-8
      # Archive all incoming emails in a file called all
      :0 c
      all
      # Discard all emails with "Out of Office" in the subject
      :0
      ,* ^Subject:.*Out of Office.*
      /dev/null
      # Process all incoming time sheets with tj3ts_receiver
      :0
      ,* ^To:.*timesheets-demo@softwareworkers\.it
      {
	:0 c:
	timesheets
	:0 w: tj3ts_receiver.lock
	| tj3ts_receiver --silent -c $PROJECTDIR/$CONFIG -d $PROJECTDIR
	:0
	failed_sheets
      }
      # Process all incoming time sheet requests with tj3ts_sender
      :0
      ,* ^To:.*timesheet-request-demo@softwareworkers\.it
      {
	ID=`formail -xSubject:`
	:0 c:
	timesheet-request
	:0 w: tj3ts_sender.lock
	| tj3ts_sender -r $ID -f --silent -c $PROJECTDIR/$CONFIG -d $PROJECTDIR
      }
      # Process all incoming status sheets with tj3ss_receiver
      :0
      ,* ^To:.*statussheets-demo@softwareworkers\.it
      {
	:0 c:
	statussheets
	:0 w: tj3ss_receiver.lock
	| tj3ss_receiver --silent -c $PROJECTDIR/$CONFIG -d $PROJECTDIR
	:0
	failed_sheets
      }
      # Process all incoming status sheet requests with tj3ss_sender
      :0
      ,* ^To:.*statussheet-request-demo@softwareworkers\.it
      {
	ID=`formail -xSubject:`
	:0 c:
	statussheet-request
	:0 w: tj3ss_sender.lock
	| tj3ss_sender -r $ID -f --silent -c $PROJECTDIR/$CONFIG -d $PROJECTDIR
      }
      # Forward a copy to project admins
      :0 c
      ! tj3-user+projectadmins@softwareworkers.it
      # Since we have archived a copy we can discard all mails here.
      :0
      /dev/null
    #+end_src
