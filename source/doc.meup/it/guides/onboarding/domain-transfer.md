# Come trasferire un dominio a Software Workers

Il modo più semplice per usufruire dei servizi `*MeUp!` di Software Workers è quello di richiedere che il proprio dominio (o più domini) siano gestiti direttamente da noi.  Nel caso il vostro dominio sia già registrato presso un altro fornitore, è necessario provvedere al suo *trasferimento* a Software Workers.

Il *trasferimento* di dominio consente a Software Workers di occuparsi per vostro conto di tutte le attività tecniche e amministrative legate alla gestione del dominio (DNS, configurazioni di sicurezza, rinnovi, ecc.) pur garantendo che la **proprietà del dominio** (ownership) rimanga sotto il vostro diretto controllo, compresa la possibilità di chiedere il trasferimento del dominio ad altro operatore nel caso lo desideraste.

Per richiedere il trasferimento di uno o più domini a Software Workers, inviate una email all'indirizzo support@xelera.eu specificando i nomi a dominio che intendete trasferire: i nostri operatori procederanno alle *opportune verifiche* e si occuperanno di avviare la procedura, notificandovi in merito all'avanzamento.

Alcuni rivenditori di domini possono fornirvi assistenza per le operazioni necessarie: in questo caso potete contattarli e chiedere loro assistenza per le operazioni elencate nei paragrafi successivi.

I registar più grossi mettono a disposizione interfacce self-service: in questo caso dovrete occuparvene direttamente voi. Ogni registrar ha la propria interfaccia e noi non siamo in grado di fornirvi la documentazione specifica per ciascuno di essi, tuttavia per i principali abbiamo redatto una mini guida:

* [trasferimento dominio da OVH](./mini-guide/transfer-domains-OVH.md)

La procedura generale per poter procedere al trasferimento è descritta paragrafi seguenti.

## Dati anagrafici del titolare

É necessario che ci forniate i seguenti dati del titolare del nome a dominio, obbligatori per poterlo registrare:

1. Tipologia cliente, scelto tra: individuo, azienda, associazione o ente pubblco
1. Nome legale dell'organizzazione o azienda
1. Nome e Cognome intestatario
1. Indirizzo completo: Città, via e numero civico, provincia, CAP, Nazione
1. Numero di telefono
1. Indirizzo email
1. Partita IVA (se applicabile)

Per i domini `.it` è inoltre necessario:

1. Codice fiscale intestatario (o national ID)
1. Tipologia organizzazione: azienda (anche individuale), libero professionista, no-profit, ente pubblico, altro ente, persona o ente straniero

Per altri Top Level Domain potrebbero essere richiesti altri dati che vi verranno comunicati ove necessario.

## Sblocco dal trasferimento

La quasi totalità dei registar (i registri dei domini) adottano sistemi di blocco del trasferimento per proteggere i domini dal rischio di furto: la prima operazione da effettuare è quindi quella di sbloccare il sistema di protezione del trasferimento del dominio.

## Indicazione del codice di trasferimento

Per poter procedere al trasferimento del dominio occorre che ci forniate il codice di trasferimento (AUTH/INFO), necessario affinché il registar cedente convalidi il trasferimento del vostro dominio.

## File di configurazione del DNS

Per poter garantire la continuità dei servizi connessi al proprio dominio è necessario che ci forniate la configurazione dei valori (record) impostati sul DNS ad esso associati.


