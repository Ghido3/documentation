---
title: Account SSO per SOFTWARE WORKERS
description: Accedere alla piattaforma online aziendale
langs: it-it
robots: noindex, nofollow 
 tags:`SOFTWARE WORKERS` `SW` `onboarding` `SSO` `SSO` `LemonLDAP::NG`

---

:::warning
Queste istruzioni sono state adattate da quelle realizzate per un progetto di alternanza scuola lavoro con un liceo; in alcune immagini appaiono i riferimenti del progetto Narratron e del Liceo Novello. A breve le immagini saranno aggiornate a quelle della piattaforma per SOFTWARE WORKERS.
:::



###### tags: `SOFTWARE WORKERS` `SW` `onboarding` `SSO` `SSO` `LemonLDAP::NG`


# Fare merge con il file "Il Single sign-on ..."


# Accedere alla piattaforma online di SOFTWARE WORKERS

Questo pad contiene tre sezioni: 
* la sezione **==1. Il contesto==** serve a capire come funziona il meccanismo del Single sign-on;
* la sezione **==2. Come creare l'account==** fornisce le istruzioni passo passo;
* la sezione **==3. Come usare l'account==** illustra i due metodi che gli utenti autenticati possono usare per accedere alle applicazioni a cui sono stati abilitati.

Le istruzioni su come creare l'account sono state scritte per essere comprese senza prerequisiti, quindi **Il contesto** può anche essere letto in un secondo momento. Se ne suggerisce comunque la lettura perché - nella prospettiva di crescente adozione dei **sistemi di identità digitale** come [SPID](https://www.spid.gov.it/) in ambito non solo nazionale ma anche [europeo](https://www.eid.gov.it/) - conoscere i concetti teorici alla base del Single sign-on faciliterà compiti che si presenteranno sempre più frequentemente nella vita quotidiana.


## 1. Il contesto

### Le applicazioni utilizzate

Per l'attività di aziendali di SW verranno utilizzati diverse applicazioni online, come:
* **Nextcloud** per condividere file, calendario, contatti, link ai siti rilevanti
* **CodiMD** per la scrittura collaborativa in markdown
* **Discourse** per il forum di discussione

Nonostante queste applicazioni siano utilizzabili anche in forma anonima, per i nostri scopi è necessario che gli utenti accedano con un account individuale, che quindi presuppone un nome utente e la relativa password. Per evitare il disagio di dover creare un account per ogni applicazione abbiamo attivato il [Single sign-on](https://it.wikipedia.org/wiki/Single_sign-on).

### L'account

L'account è lo strumento con quale vengono gestiti due diversi aspetti riguardanti gli utenti: l'autenticazione e l'autorizzazione.

#### Autenticazione

L'**autenticazione** consiste nel verificare che l'utente sia ++veramente++ chi dice di essere.
Tale verifica viene fatta attraverso:
* qualcosa che l'utente **conosce** (per esempio la password)
* qualcosa che l'utente **possiede** (per esempio un codice [OTP - One-time password](https://it.wikipedia.org/wiki/One-time_password))
* qualcosa che l'utente **è**, nel senso di caratteristiche biometriche (per esempio le impronte digitali)

L'autenticazione si definisce "forte" quando è basata sulla combinazione di più fattori, possibilmente di tipo diverso.


#### Autorizzazione

L'**autorizzazione** consiste nell'attribuire a un utente determinati diritti su programmi e dati.
Tali diritti possono ad esempio riguardare l'accesso a un servizio, l'esecuzione di un programma, la lettura, modifica, cancellazione o creazione di file e cartelle.
Il tipico processo di autorizzazione coinvolge, nella sua versione più semplice, un utente in possesso di un account (user) che chiede determinati diritti a un amministratore di sistema (admin). Ricevuta la richiesta, l'admin assegna all'account i diritti necessari. Nei casi reali spesso il processo di autorizzazione include uno o più soggetti che confermano la legittimità della richiesta fatta dall'utente: per esempio se un utente acquista un servizio a pagamento (film in streaming) l'autorizzazione è subordinata alla conferma del pagamento da parte del gestore delle carte di credito.
:::info
Per la piattaforma di SOFTWARE WORKERS gli **user** sono i soci, i dipendenti, i collaboratori continuativi (quando necessario anche i collaboratori occasionali), l'**admin** è il sistemista di SW Giovanni Biscuolo mentre Andrea Rossi - amministratore di SW - è la **persona che conferma** all'admin la legittimità delle richieste.
:::


### Come è realizzato il SSO per SOFTWARE WORKERS

#### Il nome di dominio

Per accedere alla piattaforma SOFTWARE WORKERS viene utilizzato il SSO che SOFTWARE WORKERS ha attivato per *tutti* i servizi online gestiti dall'azienda: 
* quelli a uso interno (per esempio https://pad.softwareworkers.it/), 
* quelli per i clienti (per esempio https://mail.meup.it/) 
* e quelli dei progetti speciali, come per esempio NARRATRON (progetto di alternanza scuola lavoro).

In questo modo a ogni utente che si autentica sul servizio di SSO verrà presentato un pannello con i link ai servizi abilitati per il suo account.

Poiché viene usato lo stesso sistema di SSO anche il nome del dominio è uguale per tutti, ed è stato scelto il dominio auth.meup.io


#### Il software usato per il SSO

Dopo aver valutato alcune alternative (tra cui Keycloak e Shibboleth) è stato scelto [LemonLDAP::NG](https://lemonldap-ng.org/), la cui schermata di accesso compare quindi appena si accede alla pagina di autenticazione.

## 2. Come creare l'account

### Fare la richiesta
1. Vai sulla pagina di autenticazione https://auth.meup.io/
2. [opzionale] scegli la lingua che preferisci (le istruzioni che seguono fanno riferimento all'italiano)
3. fai clic su ==Crea un account==
4. Compila la form con i dati richiesti. Il sistema genererà uno username utilizzando la prima lettera del nome e il cognome (```Nome``` ```Cognome``` diventerà la username ```ncognome```). 
Una volta compilato tutto (incluso il captcha) fai clic su ==Invia==

### Confermare i dati
5. per verificare la correttezza dell'indirizzo email che hai indicato il sistema ti invierà un'email con un link di conferma (sotto vedi un esempio dell'email)![esempio dell'email di conferma](https://i.imgur.com/0zzqhtD.png)
:::info
L'email viene inviata immediatamente, ma il mailserver del tuo provider di posta potrebbe impiegare alcune decine di minuti prima di recapitare il messaggio nella tua casella della posta in arrivo. 
Controlla anche nello spam.
:::
6. clicca sul link di conferma
7. se usi Gmail potrebbe apparire questo avviso:![link sospetto](https://i.imgur.com/haiktCo.png)
:::info
Si tratta di un [falso positivo](https://it.wikipedia.org/wiki/Falso_positivo): qui di *non affidabile* c'è solo l'algoritmo che Google utilizza per individuare i siti di phishing (vedi discussione su [Slashdot](https://tech.slashdot.org/story/17/09/30/172249/why-googles-gmail-phishing-warnings-give-false-positives))
:::
8. ignora il falso positivo e fai clic su ==procedi==

### Fare il login
9. riceverai la mail con le credenziali (username e password): ![credenziali](https://i.imgur.com/NEZEyMW.png)
Fai clic sul link per accedere al portale di autenticazione.
:::warning
se compare l'errore ==Timeout di autenticazione superato== (vedi screenshot) è sufficiente ricaricare la pagina premendo il tasto F5 sulla tastiera, o il pulsante di refresh sul browser: ![Timeout di autenticazione superato](https://i.imgur.com/gdHdPgD.png)
:::
10. **cambia subito la password!**: al primo accesso fai clic su ==Password== (il secondo pulsante da sinistra, nella barra del menu), inserire la vecchia password nel primo campo e la nuova nei due successivi: ![cambio password](https://i.imgur.com/O4KfasP.png)
11. periodicamente l'admin verifica se sono stati attivati nuovi account e li abilita all'utilizzo dei servizi; per accelerare i tempi manda una mail a support@xelera.eu segnalando la creazione del tuo nuovo account.

## 3. Come usare l'account

Dopo che il tuo account è stato abilitato potrai usarlo in due in due modi:
* con link diretto alla singola aplicazione
* passando dal portale di autenticazione

Vediamo nel dettaglio entrambe i passaggi.

### Accesso tramite link diretto

1. nella barra degli indirizzi del browser inserisci il dominio associato al servizio desiderato; per esempio ```pad.softwareworkers.it```.
:::info
Verrai reindirizzato sul portale di autenticazione
:::
2. inserisci le credenziali dell'account di SSO
3. dopo l'autenticazione il sistema ti reindirizza al sito di provenienza (```pad.softwareworkers.it```), ma questa volta sei riconosciuto come utente autorizzato e accedi al servizio.

### Accesso tramite portale di autenticazione

1. vai sul portale di autenticazione ```auth.meup.io```
2. inserisci le credenziali
3. clicca su ==Connesso come username== e poi su ==Aggiorna i miei diritti==
4. clicca sul servizio a cui vuoi accedere (per adesso ==CodiMD==, a breve aggiungeremo Nextcloud e Discourse)
:::warning
Comparirà la seguente schermata:
![errore reindirizzamento](https://i.imgur.com/XZlElXw.png)

È un problema che sarà risolto nei prossimi giorni; per adesso occorre fare clic dentro la barra degli indirizzi e cancellare i caratteri a destra dell'indirizzo ```https://pad.azionemilano.it```, come evidenziato qua sotto:
![correzione url](https://i.imgur.com/mH9uP48.png)
:::

### Dentro Nextcloud

Qualunque sia la strada - accesso diretto o tramite portale di autenticazione - alla fine sei dentro Nextcloud, che appare in questo modo:
![dentro Nextcloud](https://i.imgur.com/GIUUoz4.png)

Buon lavoro, e ricordati di **fare il logout** quando finisci!