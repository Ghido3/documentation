initfile = designedtolast.it.org

publish = publish.el
manifest = manifest.scm

src-folder = public/
dest-host = meup
dest-folder = /srv/sites/

%.el %.scm: ${initfile}
	guix shell --container emacs-no-x -- emacs --batch -l org --eval '(org-babel-tangle-file "$(<)")'

bootstrap: $(publish) $(manifest)

publish: bootstrap
	guix shell --container -m ${manifest} -- emacs --batch -l ${publish} --funcall org-publish-all

deploy:
	guix shell rsync openssh-sans-x -- rsync -az --checksum --delete --progress -e 'ssh -F secrets/config' ${src-folder} ${dest-host}:${dest-folder}

clean:
	-rm -Rf ${publish} ${manifest}

wipe-doc.meup:
	-rm -Rf public/doc.meup/* ~/.org-timestamps/doc.meup*.cache
