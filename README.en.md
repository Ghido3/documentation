# SW - Users documentazion

Public documentation and guides for Software Workers users.

Exept where otherwise stated, all content in this repository is released with the [Creative Commons Attribution-ShareAlike 4.0 International Public License](./LICENSE).
